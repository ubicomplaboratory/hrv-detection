%engine('99.203.89.93')

function exitcode = engine(base_folder,ip_folder)

% get lastest file in upload folder
dir = './';
lastest_file=getlatestfile(fullfile(dir,base_folder,ip_folder));
videoData = fullfile(dir,base_folder,ip_folder,lastest_file);
%disp(videoData);

% starting frame
startingFrame=1;
% duration sec
dur=9;

% video data properties
obj_video=VideoReader(videoData);
fs = obj_video.FrameRate;
video_height=obj_video.Height;
video_width=obj_video.Width;
video_dur=obj_video.Duration;
video_frames=round(dur*fs);

% check if face exist <- new code
faceDetector = vision.CascadeObjectDetector();
ifFaceExist = checkFaceExist(obj_video,50,faceDetector,video_frames);

if ifFaceExist
    
    % extract frames
    [videoR,videoG,videoB,videoGRAY] = VideoExtract(videoData, startingFrame, video_frames);

    % Gaussian filter (remove gaussian noise), sigma default to 1
    sigmaR=10;
    sigmaG=10;
    sigmaB=10;
    for i=1:video_frames
        videoR(:,:,i) = imgaussfilt(videoR(:,:,i), sigmaR);
        videoG(:,:,i) = imgaussfilt(videoG(:,:,i), sigmaG);
        videoB(:,:,i) = imgaussfilt(videoB(:,:,i), sigmaB);
        videoGRAY(:,:,i) = imgaussfilt(videoGRAY(:,:,i),5);
    end

    % SNR
    k=40;
    snr_var=1.3;
    [S1,S2] = SNRdetection(videoR,videoG,videoB,fs,k,snr_var);

    % cwt
    crppg = CWTsmooth(S1,fs);
    t=1:size(S1,1);
    t=t/fs;

    % result for CrPPG data
    peak_time = GetPeakTime(crppg, t, 0.5);
    rr_interval = GetRRTime(peak_time);
    [RRmean,SDRR,RMSSD] = GetHRV(peak_time);
    bpm=60*(size(peak_time,2)/(size(t,2)/fs));
    coh=GetCoherence(rr_interval,0.4,0.04,0.03);
    %% plot cwt t-f
    %crppg=crppg';
    %[wt,f]=cwt(crppg,'amor',fs);
    %surf(1:size(crppg),f,abs(wt));
    %shading interp
    
    %% output
    data.bpm=bpm;
    data.rrmean=RRmean;
    data.sdrr=SDRR;
    data.rmssd=RMSSD;
    data.coh=coh;
    outputString=jsonencode(data);
    fprintf(1,"%s\n",outputString);

else
    data.bpm=0;
    data.rrmean=0;
    data.sdrr=0;
    data.rmssd=0;
    data.coh=0;
    outputString=jsonencode(data);
    fprintf(1,"%s\n",outputString);
end

% exit code
exitcode = 0;
end

%% Custom functions
function [videoR,videoG,videoB,videoGRAY] = VideoExtract(video, startingFrame, numOfFrames)
    file = video;
    inputVideo = VideoReader(file);        
    frameCount=0;
    
    % define videoNR, videoNG, videoNB
    videoR=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoG=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoB=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoGRAY=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    
    % loop through input video
    while hasFrame(inputVideo)
        % read one frame
        frameRGB = readFrame(inputVideo);
        % count frame
        frameCount = frameCount+1;
    
        if frameCount>startingFrame 
            % cut off
            if frameCount>startingFrame+numOfFrames
                break
            end     
            % store frame to matrix
            videoR(:,:,frameCount-startingFrame)=frameRGB(:,:,1);
            videoG(:,:,frameCount-startingFrame)=frameRGB(:,:,2);
            videoB(:,:,frameCount-startingFrame)=frameRGB(:,:,3);
            videoGRAY(:,:,frameCount-startingFrame)=rgb2gray(frameRGB);
        end
    end
end

function [S1,S2] = SNRdetection(videoR,videoG,videoB,fs,k,rate)
    nrow=size(videoR,1);
    ncol=size(videoR,2); 
    nframe=size(videoR,3);
    
    %Compute SNR for each squares
    for f=1:nframe
        for i=1:nrow/k  % row
            for j=1:ncol/k   % col
                section(i,j,1,f)=mean(mean(videoR((i-1)*k+1:i*k,(j-1)*k+1:j*k,f)))/255;
                section(i,j,2,f)=mean(mean(videoG((i-1)*k+1:i*k,(j-1)*k+1:j*k,f)))/255;
                section(i,j,3,f)=mean(mean(videoB((i-1)*k+1:i*k,(j-1)*k+1:j*k,f)))/255;
            end  
        end
    end
    
    % POS
    counter=0;
    for i=1:nrow/k
        for j=1:ncol/k
            RGB=[];
            RGB(:,1)=section(i,j,1,:);
            RGB(:,2)=section(i,j,2,:);
            RGB(:,3)=section(i,j,3,:);
            RGB=RGB';
            L=20;
            H = zeros(1,size(RGB,2));
            for t =1:size(RGB,2)-L+1
                C = RGB(:,t:t+L-1);             
                SS = [0,1,-1;-2,1,1]*diag(mean(C,2))^-1*C;
                %diag(mean(C,2))
                %SS = [0,1,-1;-2,1,1]*(diag(mean(C,2))\1)*C;
                P = [1,std(SS(1,:))/std(SS(2,:))]*SS;
                Q = (P-mean(P))/std(P);
                Q(isnan(Q)) = 0;
                H(1,t:t+L-1) = H(1,t:t+L-1)+Q;
            end
            % each HS i,j has an S signal
            HH(i,j,:)=H;
            %counter=counter+1
        end
    end
    
    % fft
    N=512;
    n=0:N-1;
    f=n*fs/N;
    for i=1:nrow/k
        for j=1:ncol/k
            S=squeeze(HH(i,j,:));
            y(i,j,:)=fft(S,N);
            % band filter between 0.5-4 Hz, cut at 256th point
            tmp=squeeze(abs(y(i,j,:)));
            tmp(f<0.7)=0;
            tmp(f>1.7)=0;
            tmp=tmp(1:N/2);
            filtered_y(i,j,:)=tmp;
        end
    end
    % find fundemential frequency
    f=f;    % 512 points
    for i=1:nrow/k
        for j=1:ncol/k
            value=squeeze(filtered_y(i,j,:));   % 256 points
            valueMax=max(value);
            for p=1:size(value,1)
            if value(p)==valueMax
                break 
            end
            end
            signal=value(p)^2;%+value(p*2)^2; %+value(p*3)^2;
            noise=0;
            for q=1:size(value,1)
                if q~=p && q~=2*p
                    noise=noise+value(q)^2; 
                end
            end
            snr(i,j)=10*log10(signal/noise);
        end
    end
    % select most segnificent signal
    snr_var=var(snr(~isnan(snr)));
    snr_mean=mean(snr(~isnan(snr)));
    snr_selected=snr;
    for i=1:nrow/k
        for j=1:ncol/k
            if snr(i,j)>snr_mean+rate*snr_var
                snr_selected(i,j)=1;
            else
                snr_selected(i,j)=0;
            end  
        end
    end

    S1=squeeze(mean(mean(HH.*snr_selected)));
    S2=squeeze(mean(mean(HH.*~snr_selected)));
    %imshow(snr_selected)
end

function wt_rec = CWTsmooth(S,fs)
    [wt,f] = cwt(S,'amor',fs);
    % cut between f= 0.75 - 4 hz
    for i=1:size(f)
        if f(i)>1.7 || f(i)<0.7
            wt(i,:)=0; 
        end
    end
    % create a 0.75-4 abs wt matrix abwt 
    abswt=abs(wt);
    sumForEachFreq=sum(abswt');
    counter=0;
    for i=1:size(wt,1)
        if sumForEachFreq(i)~=0
            counter=counter+1;
            abswtNoZero(counter,:)=abswt(i,:);
            fNoZero(counter,:)=f(i,:);
        end
    end  
    % sum wt along t domain
    wt_sum=sum(abs(wt)')';
    % find maximum wt_sum entry index
    max_wt_sum=max(wt_sum);
    max_wt_sum_index=0;
    for i=1:size(f)
        if wt_sum(i)==max_wt_sum
            max_wt_sum_index=i;
        end
    end
    % let other entries to be zero in wt
    wt_filted=zeros(size(wt));
    wt_filted(max_wt_sum_index,:)=wt(max_wt_sum_index,:);
    
    % icwt with filtered wt
    wt_rec = icwt(wt_filted,'amor');
    wt_rec=wt_rec*(var(S)/var(wt_rec))^0.5;
    
end

function [RRmean,SDRR,RMSSD] = GetHRV(peak_time)
    RR=peak_time;
    n = length(RR);
    dRR = 1000*(RR(2:n)-RR(1:n-1));
    ddRR = dRR(2:n-1)-dRR(1:n-2);
    RRmean = mean(dRR);
    SDRR = std(dRR);
    RMSSD = sqrt(mean(ddRR.^2));
end

function rr_interval = GetRRTime(peak_time)
    rr_interval=[];
    for i=2:size(peak_time,2)
        rr_interval(i-1)=peak_time(i)-peak_time(i-1);
    end
end

function peak_time = GetPeakTime(pulse_wave, pulse_time, rate)
    peak_time=[];
    [peak,loc]=findpeaks(pulse_wave,'MinPeakProminence',max(pulse_wave)*rate);
    for i=1:size(loc,2)
        peak_time(i)=pulse_time(loc(i));
    end
end

function RRcoh = GetCoherence(RR,FRR_upper_boundary,FRR_lower_boundary,FRR2_range)
    % Create interpolation RR signal
    % RR unit: sec
    fs2 = 100;
    dt2 = 1/fs2;
    RRfin = sum(RR);
    qptime = 0:dt2:RRfin;
    RRtime = cumsum(RR);
    RRsignal=interp1(RRtime,RR,qptime,'PCHIP');
    RRsignal2 = RRsignal(~isnan(RRsignal));
    % Genrerate coherence value
    M = RRsignal2;
    [PRR,FRR] = periodogram(M,hamming(length(M)),length(M),1);
    PRR2 = PRR(FRR<FRR_upper_boundary&FRR>FRR_lower_boundary);
    FRR2 = FRR(FRR<FRR_upper_boundary&FRR>FRR_lower_boundary);
    [PRRpeak, peakid] = max(PRR2);
    PowerRRpeak = sum(PRR2(FRR2<FRR2(peakid)+FRR2_range &FRR2>FRR2(peakid)-FRR2_range));
    PowerRRtotal = sum(PRR2);
    RRcoh = PowerRRpeak/PowerRRtotal;
end

function disp(input)
    display(input);
end

function latestfile = getlatestfile(directory)
%This function returns the latest file from the directory passsed as input
%argument
%Get the directory contents
dirc = dir(directory);
%Filter out all the folders.
dirc = dirc(find(~cellfun(@isdir,{dirc(:).name})));
%I contains the index to the biggest number which is the latest file
[A,I] = max([dirc(:).datenum]);
    if ~isempty(I)
        latestfile = dirc(I).name;
    end
end

function outputFrame=extractFrame(videoSource,frameNumber)
    info=get(videoSource);
    videoSource.CurrentTime=(frameNumber-1)/info.FrameRate;
    outputFrame=readFrame(videoSource);
end

function isFaceExist=checkFaceExist(videoSource,num,faceDetector,total_frame_num)
    num_frame_with_face=0;
    for i=1:num
        frame_rand_num = floor(rand*total_frame_num)+1;
        frame_to_be_check=extractFrame(videoSource,frame_rand_num);
        bbox=faceDetector(frame_to_be_check);
        if size(bbox,1)~=0
            num_frame_with_face=num_frame_with_face+1;
        end
    end
    if num_frame_with_face/num>0.1
        isFaceExist = true;
    else
       isFaceExist = false;
    end
    
end
