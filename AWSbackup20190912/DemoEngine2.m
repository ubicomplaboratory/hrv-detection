%engine('upload', '184.58.198.61')
function exitcode = engine(base_folder,ip_folder)
% get lastest file in upload folder
dir = './';
lastest_file=getlatestfile(fullfile(dir,base_folder,ip_folder));
videoData = fullfile(dir,base_folder,ip_folder,lastest_file);
%disp(videoData);

% starting frame
startingFrame=1;
% duration sec
dur=9;

% video data properties
obj_video=VideoReader(videoData);
fs = obj_video.FrameRate;
video_height=obj_video.Height;
video_width=obj_video.Width;
video_dur=obj_video.Duration;
video_frames=round(dur*fs);
numOfFrames = video_frames;
Duration = dur;

% check if face exist <- new code
faceDetector = vision.CascadeObjectDetector();
ifFaceExist = checkFaceExist(obj_video,50,faceDetector,video_frames);

if ifFaceExist
% extract frames
[videoR,videoG,videoB,videoGRAY] = VideoExtract(videoData, startingFrame, video_frames);

%% skin detection
maskYcBcR = SkinDetection(videoR,videoG,videoB);
%% calculate mean
Masks = maskYcBcR;
R=zeros(numOfFrames,1);
G=zeros(numOfFrames,1);
B=zeros(numOfFrames,1);

for i=1:numOfFrames
    
    R(i)=sum(sum(videoR(:,:,i).*uint8(Masks(:,:,i))))/sum(sum(Masks(:,:,i)));
    G(i)=sum(sum(videoG(:,:,i).*uint8(Masks(:,:,i))))/sum(sum(Masks(:,:,i)));
    B(i)=sum(sum(videoB(:,:,i).*uint8(Masks(:,:,i))))/sum(sum(Masks(:,:,i)));
end
%% Nan Values

for i=1:numOfFrames
    if ~isnan(R(i))
        R(isnan(R))=R(i);
        G(isnan(G))=G(i);
        B(isnan(B))=B(i);
        break
    end
end

%%
RGB=[];
RGB(:,1)=R;
RGB(:,2)=G;
RGB(:,3)=B;
%% Amplitude selective filtering
C = RGB';
L = size(C,2);
amax = 0.002;
delta = 0.0001;
C1 = diag(mean(C,2))\C-1;
F = fft(C1,[],2)/L;
W = delta./abs(F(1,:));
W(abs(F(1,:))<amax)=1;
F1 = F.*repmat(W,[3,1]);
Chat = diag(mean(C,2))*(ifft(F1,[],2)+1);

%% POS
RGB=Chat;
L=20;
H = zeros(1,size(RGB,2));

for t =1:size(RGB,2)-L+1
    C = RGB(:,t:t+L-1);
    S = [0,1,-1;-2,1,1]*diag(mean(C,2))^-1*C;
    P = [1,std(S(1,:))/std(S(2,:))]*S;
    H(1,t:t+L-1) = H(1,t:t+L-1)+(P-mean(P))/std(P);
end
%% cwt adaptive bandpass
fs2 = 240;
[max_snr,max_p] = CWTSNR(H,fs,fs2,Duration);

%% cwt fixed p
p = max_p;
[wt_rec,wt,f,fmax,RR,DRR,RMSSD,SDRR,LFHF] = CWTSNRP(H,fs,fs2,Duration,p);

%%
rr_interval = RR/1000;
coh=GetCoherence(rr_interval,0.4,0.04,0.03);
%% plot cwt t-f
%crppg=crppg';
%[wt,f]=cwt(crppg,'amor',fs);
%surf(1:size(crppg),f,abs(wt));
%shading interp
    
%% output
data.rrmean=mean(RR);
data.bpm=60/mean(rr_interval);
data.sdrr=SDRR;
data.rmssd=RMSSD;
data.coh=coh;
outputString=jsonencode(data);
fprintf(1,"%s\n",outputString);
else
    data.bpm=0;
    data.rrmean=0;
    data.sdrr=0;
    data.rmssd=0;
    data.coh=0;
    outputString=jsonencode(data);
    fprintf(1,"%s\n",outputString);
end
exitcode = 0;
end
%% cwt p test
function [max_snr,max_p] = CWTSNR(H,fs,fs2,Duration)
%BandfreqCwt based on H signal interpolation
for ii=1:80
dt = 1/fs;
Fintime = size(H,2);
timeH = dt:dt:dt*Fintime;
dt2 = 1/fs2;
qp = 1:dt2:Duration;
y=interp1(timeH,H,qp);
S = y;
% cwt
[wt,f] = cwt(S,'amor',fs2);
wt(f>2|f<1,:)=0; 
t=1:size(S,2);
t=t/fs2;
% sum wt along t domain
wt_sum=sum(abs(wt)')';
[sumpks,sumlocs] = findpeaks(flipud(wt_sum),flipud(f),'MinPeakHeight',max(wt_sum)*0.5);
for j=1:size(sumlocs,1)
fmax = sumlocs(j);
r = 0.8;
minPeakD = r*1/fmax;
p=ii/100;
rangeMin = max(fmax-p*fmax,1);
rangeMax = min(fmax+p*fmax,2);
wt_rec = icwt(wt,f,[rangeMin,rangeMax],'SignalMean',mean(S));
SNV(j) = snr(wt_rec);
end
[SNV2, index]=max(SNV);
fmax = sumlocs(index);
SNValue(ii) = SNV2;
end
[max_snr,max_p]=max(SNValue);
max_p = max_p/100;

end
%% cwt fixed p
function [wt_rec,wt,f,fmax,RR,DRR,RMSSD,SDRR,LFHF] = CWTSNRP(H,fs,fs2,Duration,p)
%BandfreqCwt based on H signal interpolation
dt = 1/fs;
Fintime = size(H,2);
timeH = dt:dt:dt*Fintime;
dt2 = 1/fs2;
qp = 1:dt2:Duration;
y=interp1(timeH,H,qp);
S = y;
% cwt
[wt,f] = cwt(S,'amor',fs2);
wt(f>2|f<1,:)=0; 
t=1:size(S,2);
t=t/fs2;
% sum wt along t domain
wt_sum=sum(abs(wt)')';
[sumpks,sumlocs] = findpeaks(flipud(wt_sum),flipud(f),'MinPeakHeight',max(wt_sum)*0.5);
for j=1:size(sumlocs,1)
fmax = sumlocs(j,1);
r = 0.8;
minPeakD = r*1/fmax;
rangeMin = max(fmax-p*fmax,1);
rangeMax = min(fmax+p*fmax,2);
wt_rec2(j,:) = icwt(wt,f,[rangeMin,rangeMax],'SignalMean',mean(S));
SNV(j) = snr(wt_rec2(j,:));
end
[SNV2, index]=max(SNV);
fmax = sumlocs(index);
wt_rec = wt_rec2(index,:);
%[pks,locs,w,p]=findpeaks(wt_rec,qp,"MinPeakDistance",minPeakD,'MinPeakProminence',max(wt_rec)*0.5);
[pks,locs,w,p]=findpeaks(wt_rec,qp,"MinPeakDistance",minPeakD);
RR=1000*diff(locs);
DRR=diff(RR);
RMSSD = sqrt(mean(diff(RR).^2));
SDRR = std(RR);
LFHF = SDRR/RMSSD;
end
function [maskYcBcR] = SkinDetection(videoR,videoG,videoB)

    numFrames = size(videoR,3);
    height = size(videoR,1);
    width = size(videoR,2);
    maskYcBcR = false(height,width,numFrames);
    
for i=1:numFrames
    
    R = videoR(:,:,i);
    G = videoG(:,:,i);
    B = videoB(:,:,i);

    %Inverse of the Avg values of the R,G,B
    mR = 1/(mean(mean(R)));
    mG = 1/(mean(mean(G)));
    mB = 1/(mean(mean(B)));
    
    %Smallest Avg Value (MAX because we are dealing with the inverses)
    maxRGB = max(max(mR, mG), mB);
    
    %Calculate the scaling factors
    mR = mR/maxRGB;
    mG = mG/maxRGB;
    mB = mB/maxRGB;
   
    %Scale the values
     out(:,:,1) = R*mR;
     out(:,:,2) = G*mG;
     out(:,:,3) = B*mB;
     img_ycbcr = rgb2ycbcr(out);
     Cb = img_ycbcr(:,:,2);
     Cr = img_ycbcr(:,:,3);
    
    %Detect Skin
    [r,c,v] = find(Cb>=77 & Cb<=127 & Cr>=133 & Cr<=173);
    numind = size(r,1);
    bin = zeros(height,width);
    %Mark Skin Pixels
    for p=1:numind
        bin(r(p),c(p)) = 1;
    end
    maskYcBcR(:,:,i) = bin;
end

end
%% 

function fs = GetFPS(workingDir, videoName)
    file = fullfile(workingDir,videoName);
    inputVideo = VideoReader(file);    
    fs = inputVideo.FrameRate;
end
%% 

function [videoR,videoG,videoB,videoGRAY] = VideoExtract(video, startingFrame, numOfFrames)
    file = video;
    inputVideo = VideoReader(file);        
    frameCount=0;
    
    % define videoNR, videoNG, videoNB
    videoR=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoG=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoB=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoGRAY=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    
    % loop through input video
    while hasFrame(inputVideo)
        % read one frame
        frameRGB = readFrame(inputVideo);
        % count frame
        frameCount = frameCount+1;
    
        if frameCount>startingFrame 
            % cut off
            if frameCount>startingFrame+numOfFrames
                break
            end     
            % store frame to matrix
            videoR(:,:,frameCount-startingFrame)=frameRGB(:,:,1);
            videoG(:,:,frameCount-startingFrame)=frameRGB(:,:,2);
            videoB(:,:,frameCount-startingFrame)=frameRGB(:,:,3);
            videoGRAY(:,:,frameCount-startingFrame)=rgb2gray(frameRGB);
        end
    end
end
function RRcoh = GetCoherence(RR,FRR_upper_boundary,FRR_lower_boundary,FRR2_range)
    % Create interpolation RR signal
    % RR unit: sec
    fs2 = 100;
    dt2 = 1/fs2;
    RRfin = sum(RR);
    qptime = 0:dt2:RRfin;
    RRtime = cumsum(RR);
    RRsignal=interp1(RRtime,RR,qptime,"pchip");
    RRsignal2 = RRsignal(~isnan(RRsignal));
    % Genrerate coherence value
    M = RRsignal2;
    [PRR,FRR] = periodogram(M,hamming(length(M)),length(M),1);
    PRR2 = PRR(FRR<FRR_upper_boundary&FRR>FRR_lower_boundary);
    FRR2 = FRR(FRR<FRR_upper_boundary&FRR>FRR_lower_boundary);
    [PRRpeak, peakid] = max(PRR2);
    PowerRRpeak = sum(PRR2(FRR2<FRR2(peakid)+FRR2_range &FRR2>FRR2(peakid)-FRR2_range));
    PowerRRtotal = sum(PRR2);
    RRcoh = PowerRRpeak/PowerRRtotal;
end

function disp(input)
    display(input);
end

function latestfile = getlatestfile(directory)
%This function returns the latest file from the directory passsed as input
%argument
%Get the directory contents
dirc = dir(directory);
%Filter out all the folders.
dirc = dirc(find(~cellfun(@isdir,{dirc(:).name})));
%I contains the index to the biggest number which is the latest file
[A,I] = max([dirc(:).datenum]);
    if ~isempty(I)
        latestfile = dirc(I).name;
    end
end
function outputFrame=extractFrame(videoSource,frameNumber)
    info=get(videoSource);
    videoSource.CurrentTime=(frameNumber-1)/info.FrameRate;
    outputFrame=readFrame(videoSource);
end

function isFaceExist=checkFaceExist(videoSource,num,faceDetector,total_frame_num)
    num_frame_with_face=0;
    for i=1:num
        frame_rand_num = floor(rand*total_frame_num)+1;
        frame_to_be_check=extractFrame(videoSource,frame_rand_num);
        bbox=faceDetector(frame_to_be_check);
        if size(bbox,1)~=0
            num_frame_with_face=num_frame_with_face+1;
        end
    end
    if num_frame_with_face/num>0.1
        isFaceExist = true;
    else
       isFaceExist = false;
    end
    
end