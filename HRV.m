clear all; close all;

% O ~ old
% N ~ new
% M ~ mark
% E ~ Extracted

%% parameters
workingDir='~/Documents/MATLAB';
videoName='Test.avi';
outputName='outputChop.avi';

%% chop input video

% starting frame
startingFrame=1;
% desired length in sec
t=50;

fs = GetFPS(workingDir, videoName);
numOfFrames=round(t*fs);
VideoExtract(workingDir, videoName, outputName, startingFrame, numOfFrames)
% implay('outputChop.avi')
%% face detection
[facialMasks,roiMasks] = FaceDetection(workingDir, 'outputChop.avi', 'outputFace.avi');
%% ROI 

%% smoothing

%% skin detection
% masks=facialMasks;
% alpha=1;
% [frameR,frameG,frameB,maskYcBcR,frameCount,fs] = SkinDetection(masks, alpha, workingDir, 'outputFace.avi', 'outputSkin.avi');

masks=roiMasks;
alpha=1;
[frameR,frameG,frameB,maskYcBcR,frameCount,fs] = SkinDetection(masks, alpha, workingDir, 'outputROI.avi', 'outputSkin.avi');
%% normalize RGB by moving center average
averageFramesLength=30;
[normalR,normalG,normalB,frameCount,fs] = NormalizeRGB(frameR, frameG, frameB, averageFramesLength, fs, workingDir, 'normalSkin.avi');
%% upscale RGB by 8

R=normalR;
G=normalG;
B=normalB;
%%
R = interp(normalR,8);
G = interp(normalG,8);
B = interp(normalB,8);
%% compute Xs, Ys (h,w,frame) 
Xs=3*R-2*G;
Ys=1.5*R+G-1.5*B;
%% compute mean of Xs, Ys
n=size(Xs,3);
nrow=size(Xs,1);
ncol=size(Xs,2);
Xs_mean=zeros(1,n);
for i=1:n  
    counter=double(normalR(:,:,i))+double(normalG(:,:,i))+double(normalB(:,:,i));
    counter(counter~=0)=1;
    area=sum(sum(counter));
    if area==0
        Xs_mean(i)=0;
    else
        double(sum(sum(Xs(:,:,i))))
        double(area)
        double(sum(sum(Xs(:,:,i))))/double(area)
        Xs_mean(i)=double(sum(sum(Xs(:,:,i))))/double(area);
    end
end

n=size(Ys,3);
nrow=size(Ys,1);
ncol=size(Ys,2);
Ys_mean=zeros(1,n);
for i=1:n
    counter=double(normalR(:,:,i)+normalG(:,:,i)+normalB(:,:,i));
    counter(counter~=0)=1;
    area=sum(sum(counter));
    if area==0
        Ys_mean(i)=0;
    else
        Ys_mean(i)=sum(sum(Ys(:,:,i)))/area;
    end
end
%% compute band-pass filtered Xf, Yf (value)
Xf = bpfilt(Xs_mean,0.75,4,fs);
Yf = bpfilt(Ys_mean,0.75,4,fs);
%plot(Xf)
%plot(xf)
%%
Yf = interp(Yf,8);
Xf= interp(Xf,8);
%% compute S
sigXf=sqrt(var(Xf));
sigYf=sqrt(var(Yf));
a=sigXf/sigYf;
S=Xf-a*Yf;
plot(S)
legend("C-rPPG")
title("C-rPPG")
%% cwt
[wt,f] = cwt(S,'amor',fs);
s=surf(1:size(S,1),f,abs(wt));
%shading interp

frameUsed=30;


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% cwt %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cwt
[wt,f] = cwt(S,'amor',fs);
figure(4)
s=surf(1:size(S,1),f,abs(wt));
title("cwt coef on f and frame")
shading interp
wttmp=wt;
ftmp=f;

% cut between f= 0.75 - 4 hz
for i=1:size(f)
   if f(i)>4 || f(i)<0.75 
      wt(i,:)=0; 
   end
end

figure(5)
s=surf(1:size(S,1),f,abs(wt));
title("cut cwt coef on f and frame")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=1:size(S,1);
t=t/fs;

% sum wt along t domain
wt_sum=sum(abs(wt)')';
figure(6)
plot(f,wt_sum)
ylabel("sum of abs(wt)");
xlabel("f");
title("sum of wt along t");
hold on

% find maximum wt_sum entry index
max_wt_sum=max(wt_sum)
max_wt_sum_index=0;

for i=1:size(f)
    if wt_sum(i)==max_wt_sum
        max_wt_sum_index=i;
    end
end

f(max_wt_sum_index)
plot(f(max_wt_sum_index),max_wt_sum,"ro");
hold off
legend("summation of wt along t","maximum summation point");

% let other entries to be zero in wt
wt_filted=zeros(size(wt));
%wt_filted(max_wt_sum_index,:)=wt(max_wt_sum_index,:);
%max_wt_sum_index
wt_filted(32,:)=wt(32,:);
f(32)

figure(8)
s=surf(t,f,abs(wt_filted));
xlabel("t")
ylabel("f")
title("filtered wt along t and f");
% icwt with filtered wt
wt_rec = icwt(wt_filted,'amor');
wt_rec=wt_rec*(var(S)/var(wt_rec))^0.5;
figure(9)
plot(t,wt_rec)
hold on
plot(t,S-mean(S))
legend("filtered C-rPPG","orginal C-rPPG")
xlabel("t")
ylabel("power")
title("cwt filtered vs orignal C-rPPG")
hold off

% find peak's time
peak=findpeaks(wt_rec,t);
m=size(peak,2);
j=1;
peakIndex=zeros();

for i=1:size(t,2)
    if wt_rec(i)==peak(j)
        peakIndex(j)=i;
        j=j+1;
        if j>m
            break
        end
    end
end

peakTime=t(peakIndex);
csvwrite('RR.csv',peakTime)
bpm=60*size(peakTime,2)/size(t,2)

% plot R-R interval
rr=zeros();
for i=2:size(peakTime,2)
    rr(i-1)=peakTime(i)-peakTime(i-1);
end
figure(10)
plot(rr)
title("rr")

%RR = table2array(RR)
RR=peakTime;
n = length(RR)
dRR = 1000*(RR(2:n)-RR(1:n-1));
ddRR = dRR(2:n-1)-dRR(1:n-2);
RRmean = mean(dRR)
SDRR = std(dRR)
RMSSD = sqrt(mean(ddRR.^2))

%%
%%%% 2. regression of peaks, find maximum from wt
wt_est=zeros(size(wt));
wt_max=zeros(size(wt));
wt_max_index=zeros(1463,2);

for i=1:size(t)
   for j=1:size(f)
       if wt(j,i)==max(wt(:,i))
           wt_max(j,i)=wt(j,i);
           wt_max_index(i,1)=t(i);
           wt_max_index(i,2)=f(j);
       end
   end
end

plot(wt_max_index(:,1),wt_max_index(:,2),'*');
hold on
p=polyfit(wt_max_index(:,1),wt_max_index(:,2),7)
f_est=polyval(p,wt_max_index(:,1));
plot(wt_max_index(:,1),f_est)
hold off

for i=1:size(t)
    for j=1:size(f)
        if f(j)<=f_est(i)
            wt_est(j,i)=wt(j,i);
            break
        end
    end
end

surf(t,f,abs(wt_est));

% icwt with filtered wt_est
wt_rec = icwt(wt_est,'amor');
wt_rec=wt_rec*(var(S)/var(wt_rec))^0.5;
plot(t,wt_rec)
hold on
plot(t,S-mean(S))
legend("filtered C-rPPG","orginal C-rPPG")
xlabel("t")
ylabel("power")
title("cwt filtered vs orignal C-rPPG")
hold off
% find peak's time
peak=findpeaks(wt_rec,t);
m=size(peak);
j=1;

for i=1:size(t)
    if wt_rec(i)==peak(j)
        peakIndex(j)=i;
        j=j+1;
        if j>m
            break
        end
    end
end

peakTime=t(peakIndex);
csvwrite('RR.csv',peakTime)
peakTime_size=size(peakTime);
peakTime_len=peakTime_size(1);
bpm=60*peakTime_len/t(t_len)
%RR = table2array(RR)
RR=peakTime;
n = length(RR)
dRR = 1000*(RR(2:n)-RR(1:n-1));
ddRR = dRR(2:n-1)-dRR(1:n-2);
RRmean = mean(dRR)
SDRR = std(dRR)
RMSSD = sqrt(mean(ddRR.^2))



%% read in ground truth rr interval
RR_Real_Read = csvread('TrueRR.txt',1,0); % in ms
startingTime=startingFrame/fs;
samplingTime=10;
RR_Real_Read=RR_Real_Read/1000; % in sec
%%
tmp=0;
for i=1:size(RR_Real_Read,1)
    tmp=tmp+RR_Real_Read(i);
    if tmp<startingTime || tmp>startingTime+samplingTime
        RR_Real_Read(i)=0;
    end
end

RR_Real=RR_Real_Read(RR_Real_Read~=0);
plot(RR_Real,'b+');
hold on
plot(rr,'r+')
hold off
%%
function [normalR,normalG,normalB,frameCount, fs] = NormalizeRGB(frameR, frameG, frameB, averageFrameLength, fs, workingDir, outputName)
    frameCount=size(frameR,3);
    averageFrame=averageFrameLength;
    nrow=size(frameR,1);
    ncol=size(frameR,2);
    
    for i=1:frameCount
        i
        if i>(averageFrame/2) && i<=(frameCount-averageFrame/2)
            % compute working pixel points
            dotCount=0;
            for j=(i-averageFrame/2):(i+averageFrame/2)
                R=frameR(:,:,j);
                G=frameG(:,:,j);
                B=frameB(:,:,j);
                R(R~=0)=1;
                G(G~=0)=1;
                B(B~=0)=1;
                C=R+G+B;
                C(C~=0)=1;
                dotCount=dotCount+sum(sum(C));
            end
            dots=dotCount;
            % normalize
            if dots==0
                normalR(:,:,i)=double(frameR(:,:,i));
                normalG(:,:,i)=double(frameG(:,:,i));
                normalB(:,:,i)=double(frameB(:,:,i));
            else
                normalR(:,:,i)=double(frameR(:,:,i))/((sum(sum(sum(double(frameR(:,:,i-averageFrame/2:i+averageFrame/2))))))/double(dots));
                normalG(:,:,i)=double(frameG(:,:,i))/((sum(sum(sum(double(frameG(:,:,i-averageFrame/2:i+averageFrame/2))))))/double(dots));
                normalB(:,:,i)=double(frameB(:,:,i))/((sum(sum(sum(double(frameB(:,:,i-averageFrame/2:i+averageFrame/2))))))/double(dots));
            end
        end
        
        if i<=(averageFrame/2)
            % compute working pixel points
            dotCount=0;
            for j=1:(i+averageFrame/2)
                R=frameR(:,:,j);
                G=frameG(:,:,j);
                B=frameB(:,:,j);
                R(R~=0)=1;
                G(G~=0)=1;
                B(B~=0)=1;
                C=R+G+B;
                C(C~=0)=1;
                dotCount=dotCount+sum(sum(C));
            end
            dots=dotCount;
            % normalize
            normalR(:,:,i)=frameR(:,:,i)/((sum(sum(sum(frameR(:,:,1:i+averageFrame/2)))))/dots);
            normalG(:,:,i)=frameG(:,:,i)/((sum(sum(sum(frameG(:,:,1:i+averageFrame/2)))))/dots);
            normalB(:,:,i)=frameB(:,:,i)/((sum(sum(sum(frameB(:,:,1:i+averageFrame/2)))))/dots);
        end
        
        if i>(frameCount-averageFrame/2)
            % compute working pixel points
            dotCount=0;
            for j=i-averageFrame/2:frameCount
                R=frameR(:,:,j);
                G=frameG(:,:,j);
                B=frameB(:,:,j);
                R(R~=0)=1;
                G(G~=0)=1;
                B(B~=0)=1;
                C=R+G+B;
                C(C~=0)=1;
                dotCount=dotCount+sum(sum(C));
            end
            dots=dotCount;
            % normalize
            normalR(:,:,i)=frameR(:,:,i)/((sum(sum(sum(frameR(:,:,i-averageFrame/2:frameCount)))))/dots);
            normalG(:,:,i)=frameG(:,:,i)/((sum(sum(sum(frameG(:,:,i-averageFrame/2:frameCount)))))/dots);
            normalB(:,:,i)=frameB(:,:,i)/((sum(sum(sum(frameB(:,:,i-averageFrame/2:frameCount)))))/dots);
        end
        
    end 
    
    % create new output video
    outputVideo = VideoWriter(fullfile(workingDir,outputName));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:frameCount
        frameNRGB(:,:,1) = uint8(normalR(:,:,i));
        frameNRGB(:,:,2) = uint8(normalG(:,:,i));
        frameNRGB(:,:,3) = uint8(normalB(:,:,i));
    
        writeVideo(outputVideo,frameNRGB);
    end
    
end

function [frameR,frameG,frameB,maskYcBcR,frameCount,fs] = SkinDetection(maskArray, alpha, workingDir, videoName, outputName)
    inputVideo = fullfile(workingDir,videoName);
    video = VideoReader(inputVideo);
    fs=video.FrameRate;
    height=video.Height;
    width=video.Width;
    
    % define frame counter
    frameCount=0;
    
    while hasFrame(video)      
        frameCount=frameCount+1
        
        % read one frame
        frame = readFrame(video);
        frame = im2double(frame);
        mask = logical(maskArray(:,:,frameCount));
        area=sum(sum(mask));
        
        % RGB - double
        frameRGB = frame;
        
        % r,g,b normalized RGB
        r=frameRGB(:,:,1);
        g=frameRGB(:,:,2);
        b=frameRGB(:,:,3);
        
        % convert to normalized y, cR, cB
        y =	0.299.*r + 0.587.*g + 0.114.*b;
        cB = -0.168736.*r - 0.331264.*g + 0.500.*b;
        cR = 0.500.*r - 0.418688.*g - 0.081312.*b;
        
%         % filtering...
%         cBmask=true(height,width);
%         cRmask=true(height,width);
%         ymask=true(height,width);     
%         if area~=0
%             meancB=sum(sum(cB(mask)))/area;
%             meancR=sum(sum(cR(mask)))/area;
%             varcB=sum(sum((cB(mask)-meancB).^2))/(area-1);
%             varcR=sum(sum((cR(mask)-meancR).^2))/(area-1);
%             sigmacB=sqrt(varcB);
%             sigmacR=sqrt(varcR);
%             cBmask(cB<(meancB-alpha*sigmacB))=0;
%             cBmask(cB>(meancB+alpha*sigmacB))=0;
%             cRmask(cR<(meancR-alpha*sigmacR))=0;
%             cRmask(cR>(meancR+alpha*sigmacR))=0;
%             
%             cB=cB.*cBmask.*cRmask;
%             cR=cR.*cBmask.*cRmask;
%             y=y.*cBmask.*cRmask;
%         end
        
        % 8-bit Y, CR, CB
        Y = y.*219 + 16;
        CB = cB.*224 + 128;
        CR = cR.*224 + 128;
     
        % 98<=Cb<=142, 133<=Cr<=177
        CBmask1=true(height,width);
        CRmask1=true(height,width);

        CBmask1(CB<98)=0;
        CBmask1(CB>142)=0;
        CRmask1(CR<133)=0;
        CRmask1(CR>177)=0; 
        
        % filtered cR,cB,Y
        cB=cB.*CBmask1.*CRmask1;
        cR=cR.*CBmask1.*CRmask1;
        y=y.*CBmask1.*CRmask1;
        
        % show YCrCb 8bit image
        frameYCbCr(:,:,1)=uint8(Y);
        frameYCbCr(:,:,2)=uint8(CB);
        frameYCbCr(:,:,3)=uint8(CR);      
        %imshow(frameYCbCr)
        %hist(Y(mask))
        %hist(CB(mask))
        %hist(CR(mask))

        % reconstruct image
        % y, cB, cR -> r, g, b
        r = 1.0.*y + 0.*cB + 1.402.*cR;
        g = 1.0.*y - 0.344136.*cB - 0.714136.*cR;
        b =	1.0.*y + 1.772.*cB + 0.*cR;    
        % 8 bit RGB
        R=r.*255;
        G=g.*255;
        B=b.*255;
        frameTmp(:,:,1)=uint8(R);
        frameTmp(:,:,2)=uint8(G);
        frameTmp(:,:,3)=uint8(B);
        imshow(frameTmp)
        
        % output
        frameR(:,:,frameCount)=uint8(R);
        frameG(:,:,frameCount)=uint8(G);
        frameB(:,:,frameCount)=uint8(B);
        maskYcBcR(:,:,frameCount)=CBmask1.*CRmask1;
        
    end
    
    % plot hist of values
        figure(1);
        subplot(3,1,1);
        histogram(Y(mask),'Normalization','pdf');
        legend("Y");
        title("Y hist from choped area")

        subplot(3,1,2);
        histogram(CB(mask),'Normalization','pdf');
        legend("CB")
        title("CB hist from choped area")

        subplot(3,1,3);
        histogram(CR(mask),'Normalization','pdf');
        legend("CR")
        title("CR hist from choped area")
        
        hold off

    % create new output video
    outputVideo = VideoWriter(fullfile(workingDir,outputName));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:frameCount
        frameNRGB(:,:,1) = uint8(frameR(:,:,i));
        frameNRGB(:,:,2) = uint8(frameG(:,:,i));
        frameNRGB(:,:,3) = uint8(frameB(:,:,i));
    
        writeVideo(outputVideo,frameNRGB);
    end
    
end

function [masks,masksROI] = FaceDetection(workingDir, videoName, outputName)
    inputVideo = fullfile(workingDir,videoName);
    video = VideoReader(inputVideo);
    fs=video.FrameRate;

    % Create the face detector object.
    faceDetector = vision.CascadeObjectDetector();

    % Create the point tracker object.
    pointTracker = vision.PointTracker('MaxBidirectionalError', 2);
    
    % define frame counter
    frameCount=0;
    
    % define pervious face box points
    trackingXY=[];
    perviousXY=[];
    currentXY=[];
    
    while hasFrame(video) 
        
        frameCount=frameCount+1
        % read one frame
        frame = readFrame(video);
        % convert to gray scale
        frameGray = rgb2gray(frame);
        frameRGB = frame;
        % define head mask 
        mask=zeros(size(frameGray));        
        % define roi mask
        maskL=zeros(size(frameGray));
        maskR=zeros(size(frameGray));
        maskROI=zeros(size(frameGray));
        
        if size(perviousXY,1)==0
            % 1. detecting face, bbox is [x,y,h,w], boxPoints is x,y pairs
            bbox = faceDetector.step(frameGray);
            % if face detected, generating eigenface points
            if ~isempty(bbox)
                bboxPoints = bbox2points(bbox(1, :))
                % 2. inside bbox, find eigenface feature points
                points = detectMinEigenFeatures(frameGray, 'ROI', bbox(1, :));
                % 3. extract points location
                eigenXY=points.Location;
                numPts = size(eigenXY,1);
                % 4. start klt tracker
                initialize(pointTracker, eigenXY, frameGray);
                currentXY=eigenXY;
            end
            perviousXY=currentXY;
        else
            %release(pointTracker);
            %initialize(pointTracker, perviousXY, frameGray);
            [xyPoints, isFound] = step(pointTracker, frameGray);
            currentXY = xyPoints(isFound, :);
            % periviousXY must has same num with currentXY
            if size(currentXY,1)~=size(perviousXY,1)
                perviousXY = perviousXY(isFound, :);
            end
            % Estimate the geometric transformation between the old and new points.
            [xform, oldInliers, visiblePoints] = estimateGeometricTransform(perviousXY, currentXY, 'similarity', 'MaxDistance', 4);

            % Apply the transformation to the bounding box.
            bboxPoints = transformPointsForward(xform, bboxPoints);
            
            % update perviousXY
            perviousXY=currentXY;
            setPoints(pointTracker, perviousXY);
        
            % Display a bounding box around the face being tracked.
            bboxPolygon = reshape(bboxPoints', 1, []);
            
            % 5. generating mask for bbox
            maskXs=sort(round(bboxPoints(:,1)));
            maskYs=sort(round(bboxPoints(:,2)));
            mask(maskYs(1):maskYs(4),maskXs(1):maskXs(4))=1;
            mask(maskYs(2):maskYs(3),maskXs(2):maskXs(3))=0;
            maskL(maskYs(2):maskYs(3),maskXs(2):maskXs(3))=1;
            maskR(maskYs(2):maskYs(3),maskXs(2):maskXs(3))=1;
            
            % Decided if points in polygon
            % 6. generating x,y pair for points in mask
            [xq yq]=find(mask(:,:)==1);
            xv=bboxPoints(:,1);
            xv(5)=xv(1);  
            yv=bboxPoints(:,2);
            yv(5)=yv(1);
            [in,on] = inpolygon(xq,yq, yv,xv);
            inon = in; %in | on;                                 
            idx = find(inon(:));                       
            xcoord = xq(idx);                      
            ycoord = yq(idx);
            %plot(yv,xv,"b+-");
            idx = sub2ind(size(mask),xcoord,ycoord);
            mask(maskYs(1):maskYs(4),maskXs(1):maskXs(4))=0;
            mask(maskYs(2):maskYs(3),maskXs(2):maskXs(3))=1;
            mask(idx) = 1;

            % Display tracked points.
            frameRGB = insertShape(frameRGB, 'Polygon', bboxPolygon, 'LineWidth', 3);
            frameRGB = insertMarker(frameRGB, currentXY, '+', 'Color', 'white');
            
            % roi
            centerX=(maskXs(1)+maskXs(4))/2;
            centerY=(maskYs(1)+maskYs(4))/2;
            ratioWidth=0.2;
            ratioHeight=0.2;
            offsetX=0.23;
            offsetY=0.05;
            roiRX=((bboxPoints(:,1)-centerX).*ratioWidth+centerX)+offsetX*(maskXs(3)-maskXs(2));
            roiRY=((bboxPoints(:,2)-centerY).*ratioHeight+centerY)+offsetY*(maskYs(3)-maskYs(2));
            roiLX=((bboxPoints(:,1)-centerX).*ratioWidth+centerX)-offsetX*(maskXs(3)-maskXs(2));
            roiLY=((bboxPoints(:,2)-centerY).*ratioHeight+centerY)+offsetY*(maskYs(3)-maskYs(2));
            roiRX(5)=roiRX(1);
            roiRY(5)=roiRY(1);
            roiLX(5)=roiLX(1);
            roiLY(5)=roiLY(1);
            
            roibboxRPoints(:,1)=roiRX(1:4);
            roibboxRPoints(:,2)=roiRY(1:4);
            roibboxRPolygon = reshape(roibboxRPoints', 1, []);
            frameGray = insertShape(frameGray, 'Polygon', roibboxRPolygon, 'LineWidth', 1);
            frameRGB = insertShape(frameRGB, 'Polygon', roibboxRPolygon, 'LineWidth', 1);
            [xq yq]=find(maskR(:,:)==1);
            [in,on] = inpolygon(xq,yq, roiRY,roiRX);
            inon = in;
            idx = find(inon(:));                       
            xcoord = xq(idx);                      
            ycoord = yq(idx);
            idx = sub2ind(size(maskR),xcoord,ycoord);
            maskR(maskR==1)=0;
            maskR(idx) = 1;
                        
            roibboxLPoints(:,1)=roiLX(1:4);
            roibboxLPoints(:,2)=roiLY(1:4);
            roibboxLPolygon = reshape(roibboxLPoints', 1, []);
            frameGray = insertShape(frameGray, 'Polygon', roibboxLPolygon, 'LineWidth', 1);
            frameRGB = insertShape(frameRGB, 'Polygon', roibboxLPolygon, 'LineWidth', 1);
            [xq yq]=find(maskL(:,:)==1);
            [in,on] = inpolygon(xq,yq, roiLY,roiLX);
            inon = in;
            idx = find(inon(:));                       
            xcoord = xq(idx);                      
            ycoord = yq(idx);
            idx = sub2ind(size(maskL),xcoord,ycoord);
            maskL(maskL==1)=0;
            maskL(idx) = 1;
            maskROI=maskL+maskR;
            imshow(frameGray.*uint8(mask),[]);
        end
        
        % store processed output frame
        videoN1R(:,:,frameCount)=double(frame(:,:,1)).*mask;
        videoN1G(:,:,frameCount)=double(frame(:,:,2)).*mask;
        videoN1B(:,:,frameCount)=double(frame(:,:,3)).*mask;
        
        % store processed demo frame
        videoN2R(:,:,frameCount)=double(frameRGB(:,:,1));
        videoN2G(:,:,frameCount)=double(frameRGB(:,:,2));
        videoN2B(:,:,frameCount)=double(frameRGB(:,:,3));
        
        % store processed ROI frame
        videoN3R(:,:,frameCount)=double(frame(:,:,1)).*maskROI;
        videoN3G(:,:,frameCount)=double(frame(:,:,2)).*maskROI;
        videoN3B(:,:,frameCount)=double(frame(:,:,3)).*maskROI;
        
        % store facial mask frame
        masks(:,:,frameCount)=mask;
        
        % store roi mask frame
        masksROI(:,:,frameCount)=maskROI;
    end 
    
    % create new demo video
    outputVideo = VideoWriter(fullfile(workingDir,'outputDemo.avi'));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:frameCount
        frameNRGB(:,:,1) = uint8(videoN2R(:,:,i));
        frameNRGB(:,:,2) = uint8(videoN2G(:,:,i));
        frameNRGB(:,:,3) = uint8(videoN2B(:,:,i));
    
        writeVideo(outputVideo,frameNRGB);
    end

    close(outputVideo);
    
    % create new output video
    outputVideo = VideoWriter(fullfile(workingDir,outputName));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:frameCount
        frameNRGB(:,:,1) = uint8(videoN1R(:,:,i));
        frameNRGB(:,:,2) = uint8(videoN1G(:,:,i));
        frameNRGB(:,:,3) = uint8(videoN1B(:,:,i));
    
        writeVideo(outputVideo,frameNRGB);
    end

    close(outputVideo);
    
    % create new roi output video
    outputVideo = VideoWriter(fullfile(workingDir,'outputROI.avi'));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:frameCount
        frameNRGB(:,:,1) = uint8(videoN3R(:,:,i));
        frameNRGB(:,:,2) = uint8(videoN3G(:,:,i));
        frameNRGB(:,:,3) = uint8(videoN3B(:,:,i));
    
        writeVideo(outputVideo,frameNRGB);
    end

    close(outputVideo);
        
end

function fs = GetFPS(workingDir, videoName)
    file = fullfile(workingDir,videoName);
    inputVideo = VideoReader(file);    
    fs = inputVideo.FrameRate;
end

function VideoExtract(workingDir, videoName, outputName, startingFrame, numOfFrames)
    file = fullfile(workingDir,videoName);
    inputVideo = VideoReader(file);    
    fs = inputVideo.FrameRate;
    
    frameCount=0;
    
    % define videoNR, videoNG, videoNB
    videoNR=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoNG=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));
    videoNB=uint8(zeros(inputVideo.Height, inputVideo.Width, numOfFrames));

    % loop through input video
    while hasFrame(inputVideo)
        % read one frame
        frameORGB = readFrame(inputVideo);
        % count frame
        frameCount = frameCount+1;
    
        if frameCount>startingFrame 
            % cut off
            if frameCount>startingFrame+numOfFrames
                break
            end     
            % store frame to matrix
            videoNR(:,:,frameCount-startingFrame)=frameORGB(:,:,1);
            videoNG(:,:,frameCount-startingFrame)=frameORGB(:,:,2);
            videoNB(:,:,frameCount-startingFrame)=frameORGB(:,:,3);
        end
    end

    % create new video
    outputVideo = VideoWriter(fullfile(workingDir,outputName));
    outputVideo.FrameRate = fs;
    open(outputVideo);

    for i = 1:numOfFrames
        frameNRGB(:,:,1) = videoNR(:,:,i);
        frameNRGB(:,:,2) = videoNG(:,:,i);
        frameNRGB(:,:,3) = videoNB(:,:,i);
    
        writeVideo(outputVideo,frameNRGB);
    end

    close(outputVideo);
end

function y = bpfilt(signal, f1, f2, fs, isplot)
%% Bandpass filtering
%
% Syntax:
%   y = bpfilt(signal, f1, f2, [options])
%
% Description:
%   This function performs bandpass filtering of a time series 
%   with rectangle window.
%
% Input Arguments:
%   signal 	- a column vector of time series.
%   f1 		- the lower bound of frequencies (in Hz).
%   f2 		- the upper bound of frequencies (in Hz).
%
% Options:
%   fs      - the sampling frequency in Hz. Default is 1 Hz.
%   isplot  - whether to produce plots.
%
% Output Arguments:
%   y 		- the filtered time series.
%
% Examples:
%   fs = 100;
%   t  = 1:1/fs:10;
%   x  = sin(t);
%   y  = bpfilt(x,20,30);
%
% See also 
%
% References:
%
% History:
%   07/13/2016 - Initial script.
%
%__________________________________________________________________________
% Wonsang You(wsgyou@gmail.com)
% 07/13/2016
% Copyright (c) 2016 Wonsang You.
    % getting options
    if nargin < 4 || isempty(fs)
        fs = 1;
    end
    if nargin < 5 || isempty(isplot)
        isplot = 1;
    end
    % define variables
    if isrow(signal)
        signal = signal';
    end
    N  = length(signal);
    dF = fs/N;
    f  = (-fs/2:dF:fs/2-dF)';
    % Band-Pass Filter:
    if isempty(f1) || f1==-Inf
        BPF = (abs(f) < f2);
    elseif isempty(f2) || f2==Inf
        BPF = (f1 < abs(f));
    else
        BPF = ((f1 < abs(f)) & (abs(f) < f2));
    end
%{
%% Power spectrum of the band-pass filter
if isplot
    figure;
    plot(f,BPF);
    title(sprintf('Power spectrum of the band-pass filter in (%.3f, %.3f) Hz',f1,f2));
end
%}
    % Power spectrum of the original signal
    signal 	 = signal-mean(signal);
    spektrum = fftshift(fft(signal))/N;
    if isplot
        figure;
        subplot(2,1,1);
        plot(f,abs(spektrum));
        title('Power spectrum of the original signal');
    end
    % Power spectrum of the band-pass filtered signal
    spektrum = BPF.*spektrum;
    if isplot
        subplot(2,1,2);
        plot(f,abs(spektrum));
        title(sprintf('Power spectrum of the band-pass filtered signal in (%.3f, %.3f) Hz',f1,f2));
    end
    % The band-pass filtered time series
    y = ifft(ifftshift(spektrum)); %inverse ifft
    y = real(y);
    if isplot
        time = 1/fs*(0:N-1)';
	
        figure;
        subplot(2,1,1);
        plot(time,signal);
        title('The original time series');
        subplot(2,1,2);
        plot(time,y);
        title(sprintf('The band-pass filtered time series in (%.3f, %.3f) Hz',f1,f2));
    end
end